<?php

require_once dirname(__DIR__).'/src/Prepend.php';
require_once ROOTPATH . '/3rdparty/vendor/autoload.php';

class BaseTest extends \PHPUnit\Framework\TestCase
{

    protected function setup()
    {
        parent::setup();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

}