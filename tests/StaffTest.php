<?php

require_once dirname(__DIR__).'/tests/BaseTest.php';

class StaffTest extends BaseTest
{

    public function testUserIsWaiter()
    {

        $user = new Staff();
        $user->setWaiter(true);

        $this->assertTrue($user->isWaiter());

        $user = new Staff();
        $user->setWaiter(false);

        $this->assertTrue($user->isWaiter() === false);

        $user = new Staff();
        $user->setManager(true);

        $this->assertTrue($user->isWaiter() === false);

        $user = new Staff();
        $user->setManager(false);

        $this->assertTrue($user->isWaiter() === false);

    }

    public function testUserIsManager()
    {
        $user = new Staff();
        $user->setWaiter(true);

        $this->assertTrue($user->isManager() === false);

        $user = new Staff();
        $user->setWaiter(false);

        $this->assertTrue($user->isManager() === false);

        $user = new Staff();
        $user->setManager(true);

        $this->assertTrue($user->isManager() === true);

        $user = new Staff();
        $user->setManager(false);

        $this->assertTrue($user->isManager() === false);
    }

    public function testLoggedUser()
    {
        $loggedUser = new Staff(11, 'Andres', true, false);

        StaffManager::login($loggedUser);

        $usr = StaffManager::getLoggedUser();

        $this->assertTrue($usr->getId() === 11);
        $this->assertTrue($usr->getUsername() === 'Andres');
        $this->assertTrue($usr->isWaiter());
        $this->assertTrue($usr->isManager() === false);
    }

}