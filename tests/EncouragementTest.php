<?php
require_once dirname(__DIR__).'/tests/BaseTest.php';

class EncouragementTest extends BaseTest {

    protected function tearDown(){
        OrderManager::clearOrders();
        parent::tearDown();
    }

    public function testCheckBonus()
    {

        $user = new Staff(10, 'Andres', true, true);
        StaffManager::login($user);


        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Aquarius', 2.10);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order, new DateTime('2020-01-01 12:00:00'));
        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id, new DateTime('2020-01-01 12:30:00'));
        $result = OrderManager::charge($id);
        $order = OrderManager::read($id);

        $orders = OrderManager::getList(new DateTime('2020-01-01 00:00:00'), new DateTime('2020-02-01 00:00:00'));

        $bonus = BonusManager::eligibleForBonus($orders);

        $this->assertFalse($bonus);
    }


    public function testCheckBonusFiveMinutes()
    {

        $user = new Staff(10, 'Andres', true, true);
        StaffManager::login($user);


        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Aquarius', 2.10);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order, new DateTime('2020-01-01 12:00:00'));
        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id, new DateTime('2020-01-01 12:05:00'));
        $result = OrderManager::charge($id);

        $order = OrderManager::read($id);

        $orders = OrderManager::getList(new DateTime('2020-01-01 00:00:00'), new DateTime('2020-02-01 00:00:00'));

        $bonus = BonusManager::eligibleForBonus($orders);

        $this->assertTrue($bonus == false);
    }


    public function testCheckBonusFourMinutes()
    {
        $user = new Staff(10, 'Andres', true, true);
        StaffManager::login($user);

        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Aquarius', 2.10);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order, new DateTime('2020-01-01 12:00:00'));

        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id, new DateTime('2020-01-01 12:04:59'));
        $order = OrderManager::read($id);

        $this->assertTrue($order->getTimingSeconds() == (4*60)+59);

        $result = OrderManager::charge($id);
        $order = OrderManager::read($id);

        $orders = OrderManager::getList(new DateTime('2020-01-01 00:00:00'), new DateTime('2020-02-01 00:00:00'));

        $this->assertTrue(sizeof($orders) == 1, 'Debería haber 1 order y hay '.sizeof($orders));


        $bonus = BonusManager::eligibleForBonus($orders);

        $this->assertTrue($bonus);
    }


    public function testCheckBonusVariousOrders()
    {
        $user = new Staff(10, 'Andres', true, true);
        StaffManager::login($user);

        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Aquarius', 2.10);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order, new DateTime('2020-01-01 12:00:00'));

        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id, new DateTime('2020-01-01 12:04:59'));
        $order = OrderManager::read($id);


        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Cafe Capuccino', 2.70);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order, new DateTime('2020-01-01 13:00:00'));

        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id, new DateTime('2020-01-01 13:03:59'));
        $order = OrderManager::read($id);

        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Cafe Capuccino', 2.70);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order, new DateTime('2020-01-01 15:00:00'));

        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id, new DateTime('2020-01-01 15:13:59'));
        $order = OrderManager::read($id);


        $result = OrderManager::charge($id);
        $order = OrderManager::read($id);

        $orders = OrderManager::getList(new DateTime('2020-01-01 00:00:00'), new DateTime('2020-02-01 00:00:00'));

        $this->assertTrue(sizeof($orders) == 3, 'Debería haber 3 order y hay '.sizeof($orders));


        $bonus = BonusManager::eligibleForBonus($orders);

        $this->assertTrue($bonus == false);
    }


    public function testCheckBonusVariousOrdersEligible()
    {
        $user = new Staff(10, 'Andres', true, true);
        StaffManager::login($user);

        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Aquarius', 2.10);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order, new DateTime('2020-01-01 12:00:00'));

        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id, new DateTime('2020-01-01 12:04:59'));
        $order = OrderManager::read($id);


        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Cafe Capuccino', 2.70);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order, new DateTime('2020-01-01 13:00:00'));

        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id, new DateTime('2020-01-01 13:03:59'));
        $order = OrderManager::read($id);

        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Cafe Capuccino', 2.70);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order, new DateTime('2020-01-01 15:00:00'));

        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id, new DateTime('2020-01-01 15:13:59'));
        $order = OrderManager::read($id);

        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Cafe Capuccino', 2.70);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order, new DateTime('2020-01-01 15:00:00'));

        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id, new DateTime('2020-01-01 15:02:59'));
        $order = OrderManager::read($id);


        $result = OrderManager::charge($id);
        $order = OrderManager::read($id);

        $orders = OrderManager::getList(new DateTime('2020-01-01 00:00:00'), new DateTime('2020-02-01 00:00:00'));

        $this->assertTrue(sizeof($orders) == 4, 'Debería haber 4 order y hay '.sizeof($orders));

        $bonus = BonusManager::eligibleForBonus($orders);

        $this->assertTrue($bonus);
    }


    public function testCheckBonusVariousOrdersEligibleButUserIsNotManager()
    {
        $user = new Staff(10, 'Andres', true, false);
        StaffManager::login($user);

        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Aquarius', 2.10);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order, new DateTime('2020-01-01 12:00:00'));

        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id, new DateTime('2020-01-01 12:04:59'));
        $order = OrderManager::read($id);


        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Cafe Capuccino', 2.70);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order, new DateTime('2020-01-01 13:00:00'));

        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id, new DateTime('2020-01-01 13:03:59'));
        $order = OrderManager::read($id);

        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Cafe Capuccino', 2.70);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order, new DateTime('2020-01-01 15:00:00'));

        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id, new DateTime('2020-01-01 15:13:59'));
        $order = OrderManager::read($id);

        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Cafe Capuccino', 2.70);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order, new DateTime('2020-01-01 15:00:00'));

        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id, new DateTime('2020-01-01 15:02:59'));
        $order = OrderManager::read($id);


        $result = OrderManager::charge($id);
        $order = OrderManager::read($id);

        $orders = OrderManager::getList(new DateTime('2020-01-01 00:00:00'), new DateTime('2020-02-01 00:00:00'));

        $this->assertTrue(sizeof($orders) == 4, 'Debería haber 4 order y hay '.sizeof($orders));

        try{
            $bonus = BonusManager::eligibleForBonus($orders);
            $this->fail('Shouldnt execute');
        }catch(Exception $e){
            $this->assertTrue($e->getCode() == BonusManager::EXCEPTION_NOT_ENOUGH_PERMISSIONS);
        }
    }

}