<?php
require_once dirname(__DIR__).'/tests/BaseTest.php';

class OrderTest extends BaseTest {

    protected function tearDown()
    {
        parent::tearDown();
    }

    public function testNewOrder()
    {
        $user = new Staff(15, 'Alberto', false, false);
        StaffManager::login($user);

        $order = new Order();
        $this->assertTrue(sizeof($order->getLogs()) == 0);
        try{
            $id = OrderManager::create($order);
            $this->fail('This shouldnt be executed');
        }catch(Exception $e){
            $this->assertTrue($e->getCode() == OrderManager::EXCEPTION_USER_NOT_ALLOWED_TO_CREATE_ORDERS);
        }

        $user = new Staff(14, 'Adrian', true, false);
        StaffManager::login($user);

        $id = OrderManager::create($order);

        $this->assertTrue(!is_null($id));
        $this->assertTrue($id >= 0);

        $order = OrderManager::read($id);
        $this->assertTrue(!is_null($order));
        $this->assertTrue($order->getId() == $id);

        $this->assertTrue(sizeof($order->getLogs()) == 1); // Creation log.

        try{
            $order = OrderManager::read(3444);
            $this->fail('This souldnt be executed');
        }catch (Exception $e){
            $this->assertTrue($e->getCode() == OrderManager::EXCEPTION_ORDER_NOT_FOUND);
        }
    }

    public function testViewOrderMenu()
    {

        $user = new Staff(14, 'Adrian', true, false);
        StaffManager::login($user);

        $order = new Order();
        $this->assertTrue(sizeof($order->getMenu()) === 0);
        $this->assertTrue($order->getStatus() == Order::STATUS_OPENED);

        $order->addItem('CocaCola', 2);
        $order->addItem('Aquarius', 2.10);
        $order->addItem('Cafe', 1.50);

        $id = OrderManager::create($order);

        $order = OrderManager::read($id);

        $this->assertTrue($order->getStatus() == Order::STATUS_OPENED);
        $this->assertTrue(sizeof($order->getMenu()) === 3);
        $this->assertEquals($order->getTotalPrice(), 5.60);

        $order->addItem('CocaCola Zero', 2.10);

        $result = OrderManager::update($order);
        $order = OrderManager::read($id);

        $this->assertTrue(sizeof($order->getMenu()) === 4);
        $this->assertEquals($order->getTotalPrice(), 7.70, 'El price deberia ser 7.70 y es '. $order->getTotalPrice());
        $this->assertTrue(sizeof($order->getLogs()) == 1, 'Debería haber 1 log y hay '.sizeof($order->getLogs()));

    }

    public function testSendToKitchen()
    {
        $user = new Staff(14, 'Adrian', true, false);
        StaffManager::login($user);

        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Aquarius', 2.10);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order);
        $order = OrderManager::read($id);
        $this->assertTrue($order->getStatus() == Order::STATUS_OPENED);

        $result = OrderManager::sendToKitchen($id);
        $this->assertTrue($result);
        $result = OrderManager::sendToKitchen($id);
        $this->assertTrue($result);

        $order = OrderManager::read($id);
        $this->assertTrue($order->getStatus() == Order::STATUS_IN_PREPARATION);
        $this->assertTrue(sizeof($order->getLogs()) == 2);
    }

    public function testServe()
    {
        $user = new Staff(14, 'Adrian', true, false);
        StaffManager::login($user);

        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Aquarius', 2.10);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order);
        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id);

        $order = OrderManager::read($id);
        $this->assertTrue($result);
        $this->assertTrue($order->getStatus() == Order::STATUS_SERVED);
        $this->assertTrue(sizeof($order->getLogs()) == 4);
    }

    public function testFinishOrder()
    {
        $user = new Staff(14, 'Adrian', true, false);
        StaffManager::login($user);

        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Aquarius', 2.10);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order);
        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id);
        $result = OrderManager::charge($id);
        $order = OrderManager::read($id);

        $this->assertTrue($result);
        $this->assertTrue($order->getStatus() == Order::STATUS_CHARGED);
        $this->assertTrue(sizeof($order->getLogs()) == 5);
    }

    public function testComplaints()
    {
        $user = new Staff(14, 'Adrian', true, false);
        StaffManager::login($user);

        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Aquarius', 2.10);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order);
        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id);
        $this->assertTrue($order->getStatus() == Order::STATUS_SERVED);

        $result = OrderManager::returnOrder($id, 'Este es el comentario de queja', 2);
        $order = OrderManager::read($id);

        $this->assertTrue($order->getStatus() == Order::STATUS_RETURNED);
        $this->assertTrue(!is_null($order->getComplaint()));
        $this->assertTrue(($order->getComplaint())->getText() == 'Este es el comentario de queja');
        $this->assertTrue(($order->getComplaint())->getRating() == 2);

        $result = OrderManager::charge($id);
        $this->assertTrue($result);
        $order = OrderManager::read($id);
        $this->assertTrue($order->getStatus() == Order::STATUS_CHARGED);

        $this->assertTrue(sizeof($order->getLogs()) == 6);

    }

    public function testOrderTiming()
    {
        $user = new Staff(14, 'Adrian', true, false);
        StaffManager::login($user);

        $order = new Order();
        $order->addItem('CocaCola', 2);
        $order->addItem('Aquarius', 2.10);
        $order->addItem('Cafe', 1.50);
        $id = OrderManager::create($order, new DateTime('2021-01-01 12:00:00'));
        $order = OrderManager::read($id);
        $this->assertTrue(is_null($order->getTimingSeconds()));

        $result = OrderManager::sendToKitchen($id);
        $result = OrderManager::readyToServe($id);
        $result = OrderManager::serve($id, new DateTime('2021-01-01 12:05:00'));

        $order = OrderManager::read($id);

        $this->assertTrue($order->getTimingSeconds() == 5*60);

    }

}