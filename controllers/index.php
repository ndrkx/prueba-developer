<?php

require_once dirname(__DIR__).'/src/Prepend.php';


$function = (isset($_POST['fn']))?$_POST['fn']:null;
$data = (isset($_POST['data']))?$_POST['data']:null;

$response = null;

switch($function){
    case 'getmenu':
        $response = json_encode(MenuManager::getList());
    break;
    case 'neworder':
        //Esto se habría hecho previamente en la app, y el método checkstaffmember comprobaría si el usuario está logueado.
        $user = new Staff(10, 'Andres', true, false);
        StaffManager::login($user);

        $order = new Order();
        $items = json_decode($data);
        foreach($items as $item){
            $order->addItem($item->name, $item->price);
        }
        $id = OrderManager::create($order);
        $response = $id;
    break;

    default:
        $response = 'Not valid function';
    break;
}

echo $response;