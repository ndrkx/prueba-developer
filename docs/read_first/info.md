# Information
For the exercises, you will be given a few stories (see `docs/exercises`) that you will have to complete.

# UI
Let's assume we already have a working UI that lets the staff communicate with our app through a REST API.

For example, for the waiter or waitress to take an order, we'll assume they have a working application with
a UI that lets them add meals or drinks to the order and a button to send the order to the kitchen and so on...

When the waiter or waitress are going to take an order, they most likely will press a "New Order" button which will
send a request to our REST API like `POST /api/orders` that will return the new order ID or something. When the
"Add meal / drink" button is clicked, some other request like `GET /api/menu` is made to return available menu items.
And so on and so forth...

# Rules for the exercises
1. You don't need to implement any UI
2. You don't need to add a real database connection. If you need to store something, you can do it in memory,
in a file or whatever you like. Everything required to pass the test has to be in the repository.
3. Domain logic should be well tested with PHPUnit or your favorite test framework
4. You're free to add any composer package you like. This includes frameworks.
5. You're free to use the directory structure you like. You can delete, remove or rename `src` and `tests` if you want
to
6. You're welcome to send any question or doubt to Asier <asier@championgames.com>

# Record your process
This is not a must. But if you are comfortable recording your process with something like Loom (https://www.loom.com/)
it will be appreciated and will probably give us more insights. You don't need to show your face.

# How to return the exercise
To return the exercise just push all your changes to the remote origin and email Asier <asier@championgames.com> with
a link to your forked repository. Special instructions to run the tests should be included if needed.

If you recorded the process, you can email a link to the video or the video itself to the same address mentioned above.