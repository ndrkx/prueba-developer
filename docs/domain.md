# Domain description
A local restaurant wants to automate the process of taking clients' orders by giving their waiters and waitresses
tablets where they can add all the clients' drinks and meals from the menu and send the order to the kitchen instantly.

Every member of the staff will have a code to identify themselves and know who's taking the orders.

An order will be in preparation while it's in the kitchen, served when all the plates and drinks are in the clients'
table or returned. For an order to be returned, the clients' complaint must be added as a note and rated from one
to five in a severity scale. If rated 3 or more, the client must **never** be charged for the order.

For the restaurant, it is important to know the time that pass between the order is taken and the moment it is served
to be able to analyze how well or how bad they're doing. To encourage the staff to take and serve orders quickly, at
the end of the month, if at least 70% of the orders were served within the first 5 minutes, all the staff receives
a 100 € bonus check.

Only managers can check if the conditions for the bonus check are met.

# Domain concepts
## Order
An order consists of a list of items from the menu with relevant info like how much does everything costs (for
simplicity we will ignore taxes), current and previous states of the order and the time every state was assigned,
a clients' complaint and severity rate in case the order was returned to the kitchen by the client and information
about if the client was charged or not.

An order can be in the following states:
- In Preparation: When the order is being prepared in the kitchen
- Ready to serve: When the order is ready to be served from the kitchen
- Served: When the order has been served to the table
- Returned: When the client has returned one or more meals with any complaints

## Staff Member
A staff member is someone that works in the restaurant. There's waiters or waitresses and managers. Every member of the
staff is identified by just a member number or ID and they can create orders and modify the state of an order that's
not returned or charged.

Additionally, managers can check if the staff meets the requirements for the bonus check at the current moment in time
and grant new bonus checks only once a month and only if bonus's conditions are met.

Non-managers can have any number of bonus checks and only non-managers can have bonus checks.

## Menu
A menu consists of a list of items with a name and a price.

## Encouragement Bonus Check
It's a bonus check of 100 EUR workers receive when at least 70% of the last month orders were served within the first 5
minutes.