# Exercises
In this directory you will find a few images with user stories. They're ordered with the number at the start of the
file name, but you're free to do them in any order you find reasonable.

## A note about acceptance criteria
As mentioned in the `docs/read_first/info.md` you don't have to do any frontend. You can ignore any "clicking" or
frontend faced terms.
