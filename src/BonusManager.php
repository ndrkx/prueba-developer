<?php


class BonusManager {

    public const EXCEPTION_BASE_BONUS_MANAGER = 30000;
    public const EXCEPTION_NOT_ENOUGH_PERMISSIONS = self::EXCEPTION_BASE_BONUS_MANAGER + 1;

    public static function eligibleForBonus(array $orders) : bool
    {
        self::checkUserIsManager();

        $total = sizeof($orders);

        $servedWithinFirst5Minutes = [];

        foreach($orders as $order){
            if($order->getTimingSeconds() < 5*60){
                $servedWithinFirst5Minutes[] = $order;
            }
        }

        if(sizeof($servedWithinFirst5Minutes) / $total >= 0.7){
            return true;
        }

        return false;
    }



    private static function checkUserIsManager() : bool
    {
        $user = StaffManager::getLoggedUser();
        if($user->isManager() === false){
            throw new Exception('User is not a manager', self::EXCEPTION_NOT_ENOUGH_PERMISSIONS);
        }
        return true;
    }
}