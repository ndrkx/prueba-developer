<?php

class Logrow
{

    private $newStatus;
    private $staffId;
    private $date;

    public function __construct(int $newStatus, int $staffId, ?DateTime $date = null){
        $this->newStatus = $newStatus;
        $this->staffId = $staffId;

        if(is_null($date)){
            $date = new DateTime('now');
        }
        $this->date = $date;
    }


    /**
     * Get the value of newStatus
     */
    public function getNewStatus() : int
    {
        return $this->newStatus;
    }

    /**
     * Set the value of newStatus
     *
     * @return  self
     */
    public function setNewStatus(int $newStatus)
    {
        $this->newStatus = $newStatus;

        return $this;
    }

    /**
     * Get the value of staffId
     */
    public function getStaffId() : int
    {
        return $this->staffId;
    }

    /**
     * Set the value of staffId
     *
     * @return  self
     */
    public function setStaffId(int $staffId)
    {
        $this->staffId = $staffId;

        return $this;
    }

    /**
     * Get the value of date
     */
    public function getDate() : DateTime
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */
    public function setDate(DateTime $date)
    {
        $this->date = $date;

        return $this;
    }
}