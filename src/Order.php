<?php

class Order implements \JsonSerializable
{

    public const STATUS_OPENED = 0;
    public const STATUS_IN_PREPARATION = 1;
    public const STATUS_READY_TO_SERVE = 2;
    public const STATUS_SERVED = 3;
    public const STATUS_RETURNED = 4;
    public const STATUS_CHARGED = 5;

    private $id;
    private $status;
    private $menu;
    private $complaint;
    private $logs;

    public function __construct() {
        $this->status = self::STATUS_OPENED;
        $this->menu = [];
        $this->complaint = null;
        $this->logs = [];
    }

    public function JsonSerialize() {
        return get_object_vars($this);
    }


    /**
     * Get the value of id
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of status
     */
    public function getStatus() : int
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */
    public function setStatus(int $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of menu
     */
    public function getMenu() : array
    {
        return $this->menu;
    }

    /**
     * Set the value of menu
     *
     * @return  self
     */
    public function setMenu(array $menu)
    {
        $this->menu = $menu;

        return $this;
    }

    public function addItem(string $name, float $price)
    {
        $menuItem = new MenuItem($name, $price);
        $this->menu[] = $menuItem;
    }

    public function getTotalPrice() : float
    {
        $total = 0;
        foreach($this->menu as $item){
            $total += $item->getPrice();
        }
        return $total;
    }

    /**
     * Get the value of complaint
     * @return Complaint
     */
    public function getComplaint() : Complaint
    {
        return $this->complaint;
    }

    /**
     * Set the value of complaint
     *
     * @return  self
     */
    public function setComplaint(Complaint $complaint)
    {
        $this->complaint = $complaint;

        return $this;
    }

    /**
     * Get the value of logs
     */
    public function getLogs() : array
    {
        return $this->logs;
    }

    /**
     * Set the value of logs
     *
     * @return  self
     */
    public function setLogs($logs)
    {
        $this->logs = $logs;

        return $this;
    }

    public function addLog(Logrow $logrow){
        $this->logs[] = $logrow;
    }

    public function getTimingSeconds() : ?float
    {
        if(sizeof($this->logs)>0){
            $initialLog = $this->logs[0];
            $finalLog = null;
            foreach($this->logs as $log){
                if($log->getNewStatus() == Order::STATUS_SERVED){
                    $finalLog = $log;
                    continue;
                }
            }
            if(!is_null($finalLog)){
                return ($finalLog->getDate())->getTimestamp() - ($initialLog->getDate())->getTimestamp();
            }
        }
        return null;
    }
}