<?php


class Staff
{

    private $id;
    private $username;
    private $waiter;
    private $manager;

    public function __construct(?int $id = null, ?string $username = null, ?bool $waiter = false, ?bool $manager = false)
    {
        $this->id = $id;
        $this->username = $username;
        $this->waiter = $waiter;
        $this->manager = $manager;
    }

    /**
     * Get the value of id
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of username
     */
    public function getUsername() : string
    {
        return $this->username;
    }

    /**
     * Set the value of username
     *
     * @return  self
     */
    public function setUsername(string $username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get the value of waiter
     */
    public function getWaiter() : bool
    {
        return $this->waiter;
    }

    /**
     * Set the value of waiter
     *
     * @return  self
     */
    public function setWaiter(bool $waiter)
    {
        $this->waiter = $waiter;

        return $this;
    }

    /**
     * Get the value of manager
     */
    public function getManager() : bool
    {
        return $this->manager;
    }

    /**
     * Set the value of manager
     *
     * @return  self
     */
    public function setManager(bool $manager)
    {
        $this->manager = $manager;

        return $this;
    }

    public function isWaiter() : bool
    {
        return $this->getWaiter();
    }

    public function isManager() : bool
    {
        return $this->getManager();
    }
}