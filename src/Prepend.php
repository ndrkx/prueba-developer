<?php

class Prepend
{

    public function __construct() {
        self::setConstants();
        self::loadLibraries();
    }

    private static function setConstants()
    {
        defined('ROOTPATH') OR define('ROOTPATH', dirname(dirname(__FILE__)));
    }

    private static function loadLibraries()
    {

        require_once ROOTPATH . '/src/MenuItem.php';
        require_once ROOTPATH . '/src/MenuManager.php';
        require_once ROOTPATH . '/src/Order.php';
        require_once ROOTPATH . '/src/OrderManager.php';
        require_once ROOTPATH . '/src/Staff.php';
        require_once ROOTPATH . '/src/StaffManager.php';
        require_once ROOTPATH . '/src/Complaint.php';
        require_once ROOTPATH . '/src/Logrow.php';
        require_once ROOTPATH . '/src/BonusManager.php';

    }

}

new Prepend();