<?php

class OrderManager
{
    public const EXCEPTION_BASE_ORDER_MANAGER = 10000;
    public const EXCEPTION_ORDER_NOT_FOUND = self::EXCEPTION_BASE_ORDER_MANAGER + 1;
    public const EXCEPTION_USER_NOT_ALLOWED_TO_CREATE_ORDERS = self::EXCEPTION_BASE_ORDER_MANAGER + 2;
    public const EXCEPTION_ORDER_ALREADY_SERVED_RETURNED_OR_READY_TO_SERVE = self::EXCEPTION_BASE_ORDER_MANAGER + 3;
    public const EXCEPTION_ORDER_ALREADY_SERVED_OR_RETURNED = self::EXCEPTION_BASE_ORDER_MANAGER + 4;
    public const EXCEPTION_ORDER_CANT_BE_SERVED = self::EXCEPTION_BASE_ORDER_MANAGER + 5;
    public const EXCEPTION_ORDER_CANT_BE_CHARGED = self::EXCEPTION_BASE_ORDER_MANAGER + 6;
    public const EXCEPTION_ORDER_CANT_BE_RETURNED = self::EXCEPTION_BASE_ORDER_MANAGER + 7;


    private static $orders;

    public static function create(Order $order, ?DateTime $date = null) : int
    {
        //First, check if user is logged in:
        self::checkUserIsWaiter();


        if(is_null(self::$orders)){
            self::$orders = [];
        }

        $newId = sizeof(self::$orders);
        $order->setId($newId);

        //Creation log:
        $log = new Logrow(Order::STATUS_OPENED, (StaffManager::getLoggedUser())->getId(), $date);
        $order->addLog($log);

        self::$orders[$newId] = $order;
        return $newId;
    }

    public static function read(int $id) : Order
    {
        self::checkUserIsWaiter();

        if(is_array(self::$orders) && isset(self::$orders[$id])){
            return self::$orders[$id];
        }
        else throw new Exception('Order not found', self::EXCEPTION_ORDER_NOT_FOUND);
    }

    public static function update(Order $order, ?DateTime $date = null) : bool
    {
        self::checkUserIsWaiter();

        if(isset(self::$orders[$order->getId()])){
            self::$orders[$order->getId()] = $order;
            return true;
        }
        else throw new Exception('Order not found', self::EXCEPTION_ORDER_NOT_FOUND);
    }

    public static function sendToKitchen(int $id, ?DateTime $date = null) : bool
    {
        self::checkUserIsWaiter();

        $order = self::read($id);

        if($order->getStatus() == Order::STATUS_OPENED) {
            $order->setStatus(Order::STATUS_IN_PREPARATION);

            //Send to kitchen log:
            $log = new Logrow(Order::STATUS_IN_PREPARATION, (StaffManager::getLoggedUser())->getId(), $date);
            $order->addLog($log);

            self::$orders[$order->getId()] = $order;
            return true;
        }else if($order->getStatus() == Order::STATUS_IN_PREPARATION) {
            return true;
        }else{
            throw new Exception('Order is ready to serve, returned or served', self::EXCEPTION_ORDER_ALREADY_SERVED_RETURNED_OR_READY_TO_SERVE);
        }
    }

    public static function readyToServe(int $id, ?DateTime $date = null) : bool
    {
        // self::checkUserIsWaiter();

        $order = self::read($id);

        if($order->getStatus() == Order::STATUS_IN_PREPARATION){
            $order->setStatus(Order::STATUS_READY_TO_SERVE);

            //Ready to serve log:
            $log = new Logrow(Order::STATUS_READY_TO_SERVE, (StaffManager::getLoggedUser())->getId(), $date);
            $order->addLog($log);

            self::$orders[$order->getId()] = $order;
            return true;
        }else if($order->getStatus() == Order::STATUS_READY_TO_SERVE){
            return true;
        }else{
            throw new Exception('Order is returned or served', self::EXCEPTION_ORDER_ALREADY_SERVED_OR_RETURNED);
        }
    }

    public static function serve(int $id, ?DateTime $date = null) : bool
    {
        self::checkUserIsWaiter();
        $order = self::read($id);

        if($order->getStatus() == Order::STATUS_READY_TO_SERVE){
            $order->setStatus(Order::STATUS_SERVED);

            //Served log:
            $log = new Logrow(Order::STATUS_SERVED, (StaffManager::getLoggedUser())->getId(), $date);
            $order->addLog($log);

            self::$orders[$order->getId()] = $order;
            return true;
        }else if($order->getStatus() == Order::STATUS_SERVED){
            return true;
        }else{
            throw new Exception('Order is not in a valid state', self::EXCEPTION_ORDER_CANT_BE_SERVED);
        }
    }

    public static function charge(int $id, ?DateTime $date = null) : bool
    {
        self::checkUserIsWaiter();

        $order = self::read($id);

        if($order->getStatus() == Order::STATUS_SERVED || $order->getStatus() == Order::STATUS_RETURNED){
            $order->setStatus(Order::STATUS_CHARGED);
            //Charged log:
            $log = new Logrow(Order::STATUS_CHARGED, (StaffManager::getLoggedUser())->getId(), $date);
            $order->addLog($log);
            self::$orders[$order->getId()] = $order;
            return true;
        }else{
            throw new Exception('Order is not in a valid state', self::EXCEPTION_ORDER_CANT_BE_CHARGED);
        }
    }

    public static function returnOrder(int $id, string $complaintText, int $complaintRating, ?DateTime $date = null) : bool
    {
        self::checkUserIsWaiter();

        $order = self::read($id);

        if($order->getStatus() == Order::STATUS_SERVED){
            $order->setStatus(Order::STATUS_RETURNED);
            $complaint = new Complaint($complaintText, $complaintRating);
            $order->setComplaint($complaint);

            //Return log:
            $log = new Logrow(Order::STATUS_RETURNED, (StaffManager::getLoggedUser())->getId(), $date);
            $order->addLog($log);

            self::$orders[$order->getId()] = $order;
            return true;
        }else{
            throw new Exception('Order is not in a valid state', self::EXCEPTION_ORDER_CANT_BE_RETURNED);
        }
    }

    public static function getList(DateTime $initDate, DateTime $endDate) : array
    {
        $res = [];

        //only orders that are opened between init and end dates:
        foreach(self::$orders as $order){
            $logs = $order->getLogs();
            $log = $logs[0];
            if($log->getDate() >= $initDate && $log->getDate() <= $endDate){
                $res[] = $order;
            }
        }

        return $res;
    }


    private static function checkUserIsWaiter() : bool
    {
        $user = StaffManager::getLoggedUser();
        if($user->isWaiter() === false){
            throw new Exception('User is not allowed to create orders', self::EXCEPTION_USER_NOT_ALLOWED_TO_CREATE_ORDERS);
        }
        return true;
    }

    public static function clearOrders()
    {
        self::$orders = [];
    }

}