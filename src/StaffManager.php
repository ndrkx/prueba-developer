<?php


class StaffManager {

    public const EXCEPTION_STAFF_MANAGER = 20000;
    public const EXCEPTION_USER_NOT_LOGGED_IN = self::EXCEPTION_STAFF_MANAGER + 1;
    private static $loggedUser;

    public static function login(Staff $user)
    {
        self::$loggedUser = $user;
    }

    /**
     * Gets currently logged in user.
     * If there is no user logged in, throws an Exception
     */
    public static function getLoggedUser() : Staff
    {
        if(!is_null(self::$loggedUser)){
            return self::$loggedUser;
        }
        throw new Exception('No user logged in', self::EXCEPTION_USER_NOT_LOGGED_IN);
    }

    public static function checkStaffMember(int $id)
    {
        //Comprobar en el listado de staff que tuvieramos en la base de datos si el pasado por parámetro es Staff.
        return true;
    }
}