<?php

class MenuManager {

    public static function getList() : array
    {
        $res =  [];

        $res[] = new MenuItem('CocaCola', 2);
        $res[] = new MenuItem('CocaCola Light', 2.10);
        $res[] = new MenuItem('CocaCola Zero', 2.15);
        $res[] = new MenuItem('Aquarius', 1.95);
        $res[] = new MenuItem('Nestea', 1.80);

        return $res;
    }
}