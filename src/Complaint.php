<?php

class Complaint {
    private $text;
    private $rating;

    public function __construct(string $text, int $rating)
    {
        $this->text = $text;
        $this->rating = $rating;
    }


    /**
     * Get the value of text
     */
    public function getText() : string
    {
        return $this->text;
    }

    /**
     * Set the value of text
     *
     * @return  self
     */
    public function setText(string $text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get the value of rating
     */
    public function getRating() : int
    {
        return $this->rating;
    }

    /**
     * Set the value of rating
     *
     * @return  self
     */
    public function setRating(int $rating)
    {
        $this->rating = $rating;

        return $this;
    }
}